const { mongoose } = require('mongoose')

const dbConnect = async () => {
    try {
        const conn = await mongoose.connect(process.env.MONGOOSE_URL);
        if(conn.connection.readyState === 1){
            console.log('Connect DB Success')
        }else {
            console.log('Connect DB Fail')
        }
    } catch (error) {
        console.log('DB connect fail')
        console.log(error)
        throw new Error(error)
    }
}

module.exports = dbConnect
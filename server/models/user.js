const mongoose = require('mongoose'); // Erase if already required
const bcrypt = require('bcrypt')
const crypto = require('crypto');
// Declare the Schema of the Mongo model
var userSchema = new mongoose.Schema({
    firstname:{
        type:String,
        required:true
    },
    lastname:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true,
        unique:true
    },
    mobile:{
        type:String,
        required:true,
        unique:true,
    },
    password:{
        type:String,
        required:true,
    },
    role:{
        type:String,
        default:'user',
    },
    cart:{
        type: Array,
        default: []
    },
    address: [{type:mongoose.Types.ObjectId, ref: 'Address'}],
    wishlist: [{type:mongoose.Types.ObjectId, ref: 'Product'}],
    isBlocker: {
        type: Boolean,
        default: false
    },
    refreshToken: {
        type: String
    },
    passwordChangeAt: {
        type: String
    },
    passwordResetToken: {
        type: String
    },
    passwordResetxpiresToken: {
        type: String
    },
}, {
    timestamps: true
});

userSchema.pre('save', async function(next){
    if(!this.isModified('password')){
        next();
    }
    const salt = bcrypt.genSaltSync(4);
    this.password = await bcrypt.hashSync(this.password, salt)
})

userSchema.methods = {
    isCorrectPassword : async function(password){
        return await bcrypt.compareSync(password, this.password);
    },
    createPasswordChangeToken : function () {
        const resetToken = crypto.randomBytes(32).toString('hex');
        this.passwordResetToken = crypto.createHash('sha256').update(resetToken).digest('hex');
        const date = new Date();
        this.passwordResetxpiresToken = Date.now() + 15*60*1000;
        return resetToken;
    }
}
//Export the model
module.exports = mongoose.model('User', userSchema);
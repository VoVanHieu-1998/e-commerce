const bcrypt = require('bcrypt')

const hashPassword = async (password) => {
    const passwordHash =  await bcrypt.hashSync(password, bcrypt.genSaltSync(8))
    return passwordHash
}

module.exports = {
    hashPassword
}
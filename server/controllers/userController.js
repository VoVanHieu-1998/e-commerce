const User = require('../models/user')
const asyncHandler = require('express-async-handler')
const bcrypt = require('bcrypt')
const { generateAccessToken, generateRefreshToken } = require('../middlewares/jwt')
//const { hashPassword } = require('../helper')
const crypto = require('crypto')
const jwt = require('jsonwebtoken')
const sendMail = require('../ultils/sendMail')

const register = asyncHandler(async (req, res) => {
    try {
        const { email, password, firstname, lastname, mobile } = req.body
        if (!email || !password || !firstname || !lastname || !mobile) {
            return res.status(400).json({
                success: false,
                mes: 'Missing inputs'
            })
        }
        //   req.body.password = await hashPassword(password)
        const user = await User.findOne({ email });
        let newUser = null;
        if (user) {
            return res.status(400).json({
                success: false,
                mes: 'User is existed',
            })
        } else {
            newUser = await User.create(req.body)
        }

        return res.status(200).json({
            success: newUser ? true : false,
            mes: newUser ? 'Register Ok' : 'Register Fail',
            data: newUser ? newUser : null
        })
    } catch (error) {
        return res.status(400).json({
            success: false,
            mes: error.message,
        })
    }
})

const login = asyncHandler(async (req, res) => {
    try {
        const { userName, password } = req.body
        if (!userName || !password) {
            return res.status(400).json({
                success: false,
                mes: 'Missing inputs'
            })
        }

        const user = await User.findOne({
            $or: [
                { email: userName },
                { mobile: userName }
            ],
            //$and: [{lastname:userName}, {$or:[{}]}]

        })
        if (user && await user.isCorrectPassword(password)) {
            const { password, role, refreshToken, ...data } = user.toObject();
            const accessToken = generateAccessToken(user._id, role)
            const refreshTokenNew = generateRefreshToken(user._id)
            await User.findByIdAndUpdate(user._id, { refreshToken: refreshTokenNew }, { new: true })
            res.cookie('refreshToken', refreshTokenNew, { httpOnly: true, maxAge: 7 * 24 * 60 * 60 * 1000 })
            return res.status(200).json({
                success: true,
                mes: 'Login Success',
                accessToken,
                data
            })
        } else {
            return res.status(400).json({
                success: false,
                mes: 'Please register user',
            })
        }

    } catch (error) {
        return res.status(400).json({
            success: false,
            mes: error.message,
        })
    }
})

const getCurrent = asyncHandler(async (req, res) => {
    try {
        const { _id } = req.user
        if (!_id) {
            return res.status(400).json({
                success: false,
                mes: 'Missing inputs'
            })
        }
        const data = await User.findById(_id).select('-refreshToken -password -role')
        return res.status(200).json({
            success: true,
            data
        })

    } catch (error) {
        return res.status(400).json({
            success: false,
            mes: error.message
        })
    }

})

const refreshAccessToken = asyncHandler(async (req, res) => {
    try {
        const cookie = req.cookies;
        if (!cookie && !cookie.refreshToken) return res.status(401).json({
            success: false,
            mes: "No refresh token in cookies"
        })
        jwt.verify(cookie.refreshToken, process.env.JWT_SECRET, async (err, decode) => {
            if (err) return res.status(401).json({
                success: false,
                mes: "Invalid access token"
            })
            const response = await User.findOne({ _id: decode._id, refreshToken: cookie.refreshToken });
            return res.status(200).json({
                success: response ? true : false,
                newAccessToken: response ? generateAccessToken(response._id, response.role) : 'Refresh token invalid'
            })

        })
    } catch (error) {
        res.status(400).json({
            success: false,
            mes: error.message
        })
    }
})

const logout = asyncHandler(async (req, res) => {
    try {
        const cookie = req.cookies;
        if (!cookie || !cookie.refreshToken) {
            return res.status(200).json({
                success: false,
                mes: "No refresh token in cookies"
            })
        }
        // xoa refresh token db
        await User.findOneAndUpdate({ refreshToken: cookie.refreshToken }, { refreshToken: '' }, { new: true })

        //xoa refresh token trinh duyet
        res.clearCookie('refreshToken', { httpOnly: true, secure: true });
        return res.status(200).json({
            success: true,
            mes: "Logout"
        })
    } catch (error) {
        return res.status(400).json({
            success: false,
            mes: error.message
        })
    }
})

const forgotPassword = asyncHandler(async (req, res) => {
    try {
        const { email } = req.query;
        if (!email) {
            return res.status(400).json({
                success: false,
                mes: "Email is required"
            })
        }
        // xoa refresh token db
        const user = await User.findOne({ email });
        if (!user) {
            return res.status(401).json({
                success: false,
                mes: "User not found"
            })
        }
        const resetToken = user.createPasswordChangeToken();
        await user.save();

        const html = `Please click link change your password. Link expires 15 minutes. 
    <a href=${process.env.URL_SERVER}/api/user/reset-password/${resetToken}>Click here</a>`
        const data = {
            to: email,
            html
        }
        const rs = await sendMail(email, html);

        res.clearCookie('refreshToken', { httpOnly: true, secure: true });
        return res.status(200).json({
            success: true,
            mes: "Forgot password"
        })
    } catch (error) {
        return res.status(400).json({
            success: true,
            mes: error.message
        })
    }
})

const resetPassword = asyncHandler(async (req, res) => {
    try {
        const { password, token } = req.body;
        if (!token || !password) {
            return res.status(400).json({
                success: false,
                mes: "Missing input"
            })
        }
        // xoa refresh token db
        const passwordResetToken = crypto.createHash('sha256').update(token).digest('hex');
        const user = await User.findOne({ passwordResetToken, passwordResetxpiresToken: { $gt: Date.now() } })
        if (!user) {
            return res.status(400).json({
                success: false,
                mes: "User not found"
            })
        }
        user.password = password;
        user.passwordResetToken = undefined;
        user.passwordChangeAt = Date.now();
        user.passwordResetxpiresToken = undefined;
        await user.save();
        return res.status(200).json({
            success: true,
            mes: "Resert password success"
        })
    } catch (error) {
        return res.status(200).json({
            success: true,
            mes: error.message
        })
    }
})

const getUsers = asyncHandler(async (req, res) => {
    try {
        const users = await User.find().select('-refreshToken -password -role');
        return res.status(200).json({
            success: users && users.length ? true : false,
            users
        })
    } catch (error) {
        return res.status(400).json({
            success: false,
            mes: error.message
        })
    }

})

const deletedUser = asyncHandler(async (req, res) => {
    try {
        const { _id } = req.body;
        if(!_id){
            return res.status(400).json({
                success: false,
                mes: 'Missing data'
            })
        }
        const user = await User.findByIdAndUpdate({_id}, {isBlocker: true}, {new:true});
        return res.status(200).json({
            success: user ? true : false,
            deletedUser : user ? `User with email: ${user.email} deleted` : `No user deleted with email: ${user.email}`
        })
    } catch (error) {
        return res.status(400).json({
            success: false,
            mes: error.message
        })
    }

})

const updateUser = asyncHandler(async (req, res) => {
    try {
        const { _id } = req.user;
        if(!_id || Object.keys(req.body).length === 0){
            return res.status(400).json({
                success: false,
                mes: 'Missing inputs'
            })
        }
        const user = await User.findByIdAndUpdate({_id}, req.body, {new:true}).select('-password -role -refreshToken');
        return res.status(200).json({
            success: user ? true : false,
            updatedUser : user ? user : `Some thing went wrong`
        })
    } catch (error) {
        return res.status(400).json({
            success: false,
            mes: error.message
        })
    }

})

const updateUserByAdmin = asyncHandler(async (req, res) => {
    try {
        const { uid } = req.body;
        if(!uid || Object.keys(req.body).length === 0){
            return res.status(400).json({
                success: false,
                mes: 'Missing inputs'
            })
        }
        const user = await User.findByIdAndUpdate({_id:uid}, req.body, {new:true}).select('-password -role -refreshToken');
        return res.status(200).json({
            success: user ? true : false,
            updatedUser : user ? user : `Some thing went wrong`
        })
    } catch (error) {
        return res.status(400).json({
            success: false,
            mes: error.message
        })
    }

})

module.exports = {
    register,
    login,
    getCurrent,
    refreshAccessToken,
    logout,
    forgotPassword,
    resetPassword,
    getUsers,
    deletedUser,
    updateUser,
    updateUserByAdmin
}
const router = require('express').Router();
const userController = require('../controllers/userController')
const {verifyAccessToken, isAdmin} = require('../middlewares/verifyToken')

router.post('/register', userController.register)
router.post('/login', userController.login)
router.post('/refreshtoken', userController.refreshAccessToken)
router.get('/forgotpassword', userController.forgotPassword)
router.post('/reset-passsword', userController.resetPassword)

router.use(verifyAccessToken)
router.get('/current', userController.getCurrent)
router.post('/logout', userController.logout)
router.post('/update-user', userController.updateUser)

router.use(isAdmin)
router.get('/get-users', userController.getUsers)
router.post('/delete-user', userController.deletedUser)
router.post('/admin-update-user', userController.updateUserByAdmin)

module.exports = router
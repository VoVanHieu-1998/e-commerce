const nodemailer = require('nodemailer')
const asyncHandler = require('express-async-handler')

const sendMail = asyncHandler(async (email, html) => {
    const transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        secure: true,
        auth: {
          user: process.env.EMAIL_NAME,
          pass: process.env.EMAIL_APP_PASSWORD
        }
      });
      
    const info = await transporter.sendMail({
        from: '"Admin store" <no-reply@store.com>',
        to: email,
        subject: "Forgot password",
        html
    });
    return info;
        
      
})

module.exports = sendMail